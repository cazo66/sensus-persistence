/*
This file is part of sensus-persistence (SessionFactory on Hibernate).

Sensus-persistence is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sensus-persistence is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with sensus-persistence.  If not, see <http://www.gnu.org/licenses/>.
*/
package br.com.ziben.persistence;

import java.io.File;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 * Class for things about Hibernate Factoring and sessions
 * @author ccardozo
 * @author rgmonachesi
 *
 */
public class HibernateFactory {
	
	//https://www.programcreek.com/java-api-examples/index.php?source_dir=cloudtm-data-platform-master/src/Hibernate-Search/hibernate-search-orm/src/test/java/org/hibernate/search/test/directoryProvider/MultipleSFTestCase.java
	
    private static HashMap<String, SessionFactory> sessionFactoryMap;
    private static ServiceRegistry serviceRegistry;
    private static Logger log = Logger.getLogger(HibernateFactory.class);

    private static String defaultSessionFactoryName = "default";
    
    
    /**
     * Build a new Singleton SessionFactory
     * @return a session factory
     * @throws HibernateException
     */
    public static SessionFactory buildSessionFactory() throws HibernateException {
    	return buildSessionFactory(defaultSessionFactoryName, null);
    }

    /**
     * Build a new Singleton SessionFactory
     * @return a session factory
     * @throws HibernateException
     */
    public static SessionFactory buildSessionFactory(String sessionFactoryName, HashMap<String, String> mapConfig) throws HibernateException {
    	if (sessionFactoryMap != null) {
	        if (sessionFactoryMap.get(sessionFactoryName) != null) {
	        	log.info(">> HibernateFactory.buildSessionFactory()->closeFactory(sessionFactoryName)");
	            closeFactory(sessionFactoryName);
	        }
    	} else {
    		sessionFactoryMap = new HashMap<String, SessionFactory>();
    	}
        return configureSessionFactory(sessionFactoryName, mapConfig);
    }
    
    /**
     * Build a SessionFactory, if not created.
     */
    public static SessionFactory buildIfNeeded() throws DataAccessLayerException {
    	return buildIfNeeded(defaultSessionFactoryName, null);
    }
    
    /**
     * Build a SessionFactory, if not created.
     */
    public static SessionFactory buildIfNeeded(String sessionFactoryName, HashMap<String, String> mapConfig) throws DataAccessLayerException {
    	if (sessionFactoryMap == null) {
    		sessionFactoryMap = new HashMap<String, SessionFactory>();
    	}
        if (sessionFactoryMap.get(sessionFactoryName) != null) {
        	log.info(">> HibernateFactory.buildIfNeeded(sessionFactoryName) sessionFactory != null");
            return sessionFactoryMap.get(sessionFactoryName);
        }
        
        try {
        	log.info(">> HibernateFactory.buildIfNeeded(sessionFactoryName) return configureSessionFactory()");
            return configureSessionFactory(sessionFactoryName, mapConfig);
        } catch (HibernateException e) {
            throw new DataAccessLayerException(e);
        }
    }
    
    public static SessionFactory getSessionFactory() {
    	log.info(">> HibernateFactory.getSessionFactory() return sessionFactory");
        return getSessionFactory(defaultSessionFactoryName);
    }
    
    public static SessionFactory getSessionFactory(String sessionFactoryName) {
    	log.info(">> HibernateFactory.getSessionFactory(sessionFactoryName) return sessionFactory");
        return sessionFactoryMap.get(sessionFactoryName);
    }

    public static Session openSession() throws HibernateException {
        buildIfNeeded();
    	log.info(">> HibernateFactory.openSession(): return sessionFactory.openSession()");
        return openSession(defaultSessionFactoryName, null);
    }

    public static Session openSession(String sessionFactoryName, HashMap<String,String> mapConfig) throws HibernateException {
        buildIfNeeded(sessionFactoryName, mapConfig);
    	log.info(">> HibernateFactory.openSession(sessionFactoryName): return sessionFactory.openSession()");
        return sessionFactoryMap.get(sessionFactoryName).openSession();
    }
    
    public static void closeFactory() {
    	log.info(">> HibernateFactory.closeFactory() sessionFactory != null; sessionFactory.close()");
    	closeFactory(defaultSessionFactoryName);
    }
    
    public static void closeFactory(String sessionFactoryName) {
        if (sessionFactoryMap.get(sessionFactoryName) != null) {
            try {
            	log.info(">> HibernateFactory.closeFactory(sessionFactoryname) sessionFactory != null; sessionFactory.close()");
                sessionFactoryMap.get(sessionFactoryName).close();
            } catch (HibernateException ignored) {
                log.error("Impossible to close the SessionFactory", ignored);
            }
        }
    }
    
    public static void close(Session session) {
        if (session != null) {
            try {
            	log.info(">> HibernateFactoryclose() session.close()");
                session.close();
            } catch (HibernateException ignored) {
                log.error("Impossible to close a Session", ignored);
            }
        }
    }

    public static void rollback(Transaction tx) {
        try {
            if (tx != null) {
            	log.info(">> rollback(): tx.rollback()");
                tx.rollback();
            }
        } catch (HibernateException ignored) {
            log.error("Impossible to rollback the Transaction", ignored);
        }
    }
    
    /**
     * Configure a session factory using a configuration file (hibernate.cfg.xml).
     * If you not provider a path by this file, it will try find where the app starts.
     * So, configure the environment variable "persistence.configuration" point to it.
     * @return
     * @throws HibernateException
     */
    private static SessionFactory configureSessionFactory(String sessionFactoryName, HashMap<String, String> mapConfig) throws HibernateException {
		log.info(">> HibernateFactory.configureSessionFactory(sessionFactoryName)");

		try {

			Configuration configuration = new Configuration();
			
			if (mapConfig == null){
				String nomeArquivo = System.getProperty("persistence.configuration");
				if (nomeArquivo == null) {
					nomeArquivo = "./" + "hibernate.cfg.xml";
					// Vamos exportar para que ninguém mais precise se preocupar.....
					System.setProperty("persistence.configuration", nomeArquivo);
					log.debug(">> configureSessionFactory(): configuring the factory from: " + nomeArquivo);
				}
				log.debug(">> configureSessionFactory() configuration file: " + nomeArquivo);
				File configFile = new File(nomeArquivo);
				configuration.configure(configFile);
			} else{
				for (String chave : mapConfig.keySet()) {
					configuration.setProperty(chave, mapConfig.get(chave));
				}
			}
			
			serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
			sessionFactoryMap.put(sessionFactoryName, configuration.buildSessionFactory(serviceRegistry));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			log.info("<< HibernateFactory.configureSessionFactory()");
		}

        return sessionFactoryMap.get(sessionFactoryName);
    }
    
}
